// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGame.h"
#include "../Public/SMyMainMenuUI.h"
#include "Engine.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMainMenuUI::Construct(const FArguments& InArgs)
{
	MainMenuHUD = InArgs._MainMenuHUD;
	
	ChildSlot
	[
		SNew(SOverlay) + SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)[
			SNew(STextBlock).ColorAndOpacity(FLinearColor::White)
							.ShadowColorAndOpacity(FLinearColor::Black)
							.ShadowOffset(FIntPoint(-1,1))
							.Font(FSlateFontInfo("Arial", 26))
							.Text(FText::FromString("Main Menu"))
		] + SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)[
			SNew(SVerticalBox) + SVerticalBox::Slot()[
				SNew(SButton).Text(FText::FromString("Play button"))
					.OnClicked(this, &SMainMenuUI::PlayGameClicked)
			] + SVerticalBox::Slot()[
				SNew(SButton).Text(FText::FromString("Exit game"))
					.OnClicked(this, &SMainMenuUI::QuitGameClicked)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SMainMenuUI::PlayGameClicked() {
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("PlayGameClicked"));

	return FReply::Handled();
}

FReply SMainMenuUI::QuitGameClicked() {
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("QuitGameClicked"));
	}


	return FReply::Handled();
}