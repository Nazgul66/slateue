// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGame.h"
#include "MyGameGameMode.h"
#include "Public/MainMenuHUD.h"

AMyGameGameMode::AMyGameGameMode(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	HUDClass = AMainMenuHUD::StaticClass();
}

