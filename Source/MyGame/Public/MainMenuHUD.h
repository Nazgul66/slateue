// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MainMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class MYGAME_API AMainMenuHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

	virtual void PostInitializeComponents()override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Menu|Main Menu")
		void PlayGameClicked();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Menu|Main Menu")
		void QuitGameClicked();

	TSharedPtr<class SMainMenuUI> MainMenuUI;
};
